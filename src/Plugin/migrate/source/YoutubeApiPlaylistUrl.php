<?php

namespace Drupal\migrate_youtube\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source plugin to query playlistItems Youtube API endpoint.
 *
 * @see https://developers.google.com/youtube/v3/docs
 *
 * @MigrateSource(
 *   id = "migrate_youtube_api_playlist"
 * )
 */
class YoutubeApiPlaylistUrl extends YoutubeApiUrl {

  /**
   * Youtube account id.
   *
   * @var string
   *  Id.
   */
  protected $accountId;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {

    // Récupération des playlists:
    if (!isset($configuration['account_id'])) {
      throw new MigrateException('The account_id config key is required to retrieve playlists');
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritDoc}
   */
  public function getUrls() {
    return [$this->getPlaylistsUrl($this->configuration['account_id'])];
  }

  /**
   * Retrieve playlists from account (id).
   *
   * @param int $accountId
   *   Youtube account ID.
   *
   * @return string
   *   Youtube API call url.
   */
  protected function getPlaylistsUrl($accountId) {
    // The max nb of results allowed per page is 50, else we need page tokens.
    return "{$this->endpoint}/playlists?part=snippet,contentDetails&channelId=$accountId&key={$this->apiKey}&maxResults=50";
  }

  /**
   * Retrieve playlist items API url.
   *
   * @param int $playlistId
   *   Youtube playlist ID.
   *
   * @return string
   *   Youtube API called url.
   */
  protected function getPlaylistItemsUrl($playlistId) {
    return "{$this->endpoint}/playlistItems?part=snippet&playlistId=$playlistId&key={$this->apiKey}&maxResults=50";
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Retrieve video IDs for each playlist.
    $playlistVideosIds = $this->getIdsFromUrlData($row->getSource()['id']);
    $row->setSourceProperty('videos', $playlistVideosIds);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritDoc}
   */
  public function getIds(): array {
    return [
      'id' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * Extra API call.
   *
   * @param int $playlistId
   *   Playlist item ID.
   *
   * @return mixed[]
   *   Data parser wrapper
   */
  public function getIdsFromUrlData($playlistId) {

    $playlistVideosUrl = $this->getPlaylistItemsUrl($playlistId);

    /* @phpstan-ignore-next-line */
    $configuration = $this->configuration + [
      'urls' => [$playlistVideosUrl],
    ];
    /* @phpstan-ignore-next-line */
    $dataParser = \Drupal::service('plugin.manager.migrate_plus.data_parser')
      ->createInstance($this->dataParser, $configuration);

    return $dataParser->getVideoIds($playlistVideosUrl);
  }

}
