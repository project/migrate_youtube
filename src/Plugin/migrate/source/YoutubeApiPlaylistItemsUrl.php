<?php

namespace Drupal\migrate_youtube\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Source plugin to query playlistItems Youtube API endpoint.
 *
 * @see https://developers.google.com/youtube/v3/docs
 *
 * @MigrateSource(
 *   id = "migrate_youtube_api_playlist_items"
 * )
 */
class YoutubeApiPlaylistItemsUrl extends YoutubeApiUrl {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {

    // Videos retrieving:
    if (!isset($configuration['playlist_id'])) {
      throw new MigrateException('The playlist_id config key is required to retrieve playlists');
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritDoc}
   */
  public function getUrls() {
    return [$this->getPlaylistItemsUrl($this->configuration['playlist_id'])];
  }

  /**
   * Retrieve videos (playlist item) from playlist API url.
   *
   * @param int $videoId
   *   Youtube playlist item ID.
   *
   * @return string
   *   Youtube API called url.
   */
  protected function getVideoItemsUrl($videoId) {
    return "{$this->endpoint}/videos?part=contentDetails&part=snippet&id=$videoId&key={$this->apiKey}";
  }

  /**
   * Retrieve playlist items API url.
   *
   * @param int $playlistId
   *   Youtube playlist ID.
   *
   * @return string
   *   Youtube API called url.
   */
  protected function getPlaylistItemsUrl($playlistId) {
    return "{$this->endpoint}/playlistItems?part=snippet&playlistId=$playlistId&key={$this->apiKey}&maxResults=50";
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // Retrieve content details for videos only if needed.
    // Extras API calls required by "contentDetails: true" in source config.
    // Warning: idlist filter won't restrict the number of API calls below.
    // Use contentDetails: false to limit calls if below properties not needed.
    if (
      isset($this->configuration['contentDetails']) &&
      $this->configuration['contentDetails']
    ) {
      $contentDetails = $this->getContentDetails($row->getSource()['video_id']);
      $row->setSourceProperty('duration', $contentDetails['duration']);
      $row->setSourceProperty('licensedContent', $contentDetails['licensedContent']);
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritDoc}
   */
  public function getIds(): array {
    return [
      'video_id' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFields(): array {
    return array_merge(parent::getFields(), [
      [
        'name' => 'video_id',
        'label' => 'Video ID',
        'selector' => 'snippet.resourceId.videoId',
      ],
      [
        'name' => 'playlist_id',
        'label' => 'Playlist ID',
        'selector' => 'snippet.playlistId',
      ],
    ]);
  }

  /**
   * Extra API call.
   *
   * @param int $videoId
   *   Playlist item ID.
   *
   * @return mixed[]
   *   Data parser wrapper
   */
  public function getContentDetails($videoId) {

    $videoUrl = $this->getVideoItemsUrl($videoId);

    /* @phpstan-ignore-next-line */
    $dataParser = \Drupal::service('plugin.manager.migrate_plus.data_parser')
      ->createInstance($this->dataParser, $this->configuration + [
        'urls' => [$videoUrl],
      ]
    );

    return $dataParser->getContentDetails($videoUrl);
  }

}
