<?php

namespace Drupal\migrate_youtube\Plugin\migrate\source;

use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_plus\Plugin\migrate\source\Url;

/**
 * Base Source class for migrate_youtube plugins.
 *
 * @see https://developers.google.com/youtube/v3/docs
 *
 * @see YoutubeApiPlaylistItemsUrl (videos)
 * @see YoutubeApiPlaylistUrl (playlists)
 */
class YoutubeApiUrl extends Url {

  /**
   * Data parser.
   *
   * @var string
   *  Plugin Id.
   */
  protected $dataParser = 'migrate_youtube_json';

  /**
   * Youtube API base endpoint.
   *
   * @var string
   *  Remote url.
   */
  protected $endpoint = 'https://www.googleapis.com/youtube/v3';

  /**
   * Youtube API key.
   *
   * @var string
   *  API key.
   */
  protected $apiKey;

  /**
   * Source plugin configuration.
   *
   * @var mixed[]
   *   Config.
   */
  protected $configuration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {

    $this->apiKey = Settings::get('youtube_api_key');

    if (!$this->apiKey) {
      throw new MigrateException('Missing youtube API key.');
    }

    $this->configuration = $configuration += $this->getBaseConfiguration();

    $configuration['urls'] = $this->getUrls();

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * List of urls for the Json plugin.
   *
   * @return String[]
   *   Array of url strings.
   */
  public function getUrls() {
    return [];
  }

  /**
   * List of mappable fields in configuration.
   *
   * @return mixed[]
   *   Base array of fields description.
   */
  public function getFields(): array {
    return [
      [
        'name' => 'id',
        'label' => 'Internal Youtube API item ID',
        'selector' => 'id',
      ],
      [
        'name' => 'title',
        'label' => 'Video title on Youtube',
        'selector' => 'snippet.title',
      ],
      [
        'name' => 'description',
        'label' => 'Description',
        'selector' => 'snippet.description',
      ],
      [
        'name' => 'published_date',
        'label' => 'Published date',
        'selector' => 'snippet.publishedAt',
      ],
    ];
  }

  /**
   * Get configuration keys common to all Youtube API migrate configs.
   *
   * @return array|mixed
   *   Source plugin config.
   */
  public function getBaseConfiguration() {

    $configuration = [];

    $configuration['item_selector'] = 'items';
    $configuration['data_parser_plugin'] = $this->dataParser;
    $configuration['data_fetcher_plugin'] = 'file';
    $configuration['fields'] = $this->getFields();
    $configuration['ids'] = $this->getIds();

    $configuration['constants'] = [
      'youtube_video_base_url' => 'http://youtube.com/watch?v=',
    ];

    return $configuration;
  }

}
