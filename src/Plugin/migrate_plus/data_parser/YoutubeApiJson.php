<?php

namespace Drupal\migrate_youtube\Plugin\migrate_plus\data_parser;

use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;

/**
 * Obtain JSON data for migration.
 *
 * @DataParser(
 *   id = "migrate_youtube_json",
 *   title = @Translation("Youtube JSON")
 * )
 */
class YoutubeApiJson extends Json {

  /**
   * Keep track of pagination advancement in successive API calls.
   *
   * @var int
   */
  private int $currentPageToken = 0;

  /**
   * Array filled iteratively.
   *
   * @var array<mixed>
   *   Response results.
   */
  private $outputData = [];

  /**
   * Retrieve list of requested videos id.
   *
   * @param string $url
   *   A Youtube API playlistItems endpoint url.
   *
   * @return mixed[]
   *   List of videos ids returned by API.
   */
  public function getVideoIds($url) {
    $sourceData = $this->getSourceData($url);
    if (empty($sourceData) || $sourceData[0]['kind'] !== 'youtube#playlistItem') {
      throw new MigrateException('Invalid Youtube API playlistItems url.');
    }
    return array_map(fn($data) => [
      'id' => $data['snippet']['resourceId']['videoId'],
    ], $sourceData);
  }

  /**
   * Retrieve contentDetails array from Youtube API video endpoint.
   *
   * @param string $videoUrl
   *   Youtube API video endpoint.
   *
   * @return mixed[]
   *   Video (playlist item) extended data.
   */
  public function getContentDetails($videoUrl) {
    $sourceData = $this->getSourceData($videoUrl);
    return $sourceData[0]['contentDetails'];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  protected function getSourceData(string $url): array {

    $response = $this->getDataFetcherPlugin()->getResponseContent($url);

    // Convert objects to associative arrays.
    $source_data = json_decode($response, TRUE, 512, JSON_THROW_ON_ERROR);

    // If json_decode() has returned NULL, it might be that the data isn't
    // valid utf8 - see http://php.net/manual/en/function.json-decode.php#86997.
    if (is_null($source_data)) {
      $utf8response = utf8_encode($response);
      $this->outputData = json_decode($utf8response, TRUE, 512, JSON_THROW_ON_ERROR);
    }

    // Otherwise, we're using xpath-like selectors.
    $selectors = explode('/', trim((string) $this->itemSelector, '/'));
    foreach ($selectors as $selector) {
      if (is_array($source_data) && array_key_exists($selector, $source_data)) {
        $this->outputData = array_merge($this->outputData, $source_data[$selector]);
      }
    }

    // Iterate through response pages and keep the resulting data in one array.
    if (isset($source_data['nextPageToken']) && !empty($source_data['nextPageToken'])) {
      if (!$this->currentPageToken) {
        $nextUrl = $url . '&pageToken=' . $source_data['nextPageToken'];
      }
      else {
        $nextUrl = preg_replace('/&pageToken=.*$/', '&pageToken=' . $source_data['nextPageToken'], $url);
      }
      $this->currentPageToken++;
      $this->getSourceData($nextUrl);
    }

    return $this->outputData;
  }

  /**
   * {@inheritdoc}
   */
  protected function openSourceUrl(string $url): bool {
    // (Re)open the provided URL.
    $source_data = $this->getSourceData($url);
    $source_data = array_filter($source_data, function ($data) {
      if (!isset($data['contentDetails'])) {
        return TRUE;
      }
      return $data['contentDetails']['itemCount'] ? TRUE : FALSE;
    });
    $this->iterator = new \ArrayIterator($source_data);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchNextRow(): void {
    $current = $this->iterator->current();
    if ($current) {
      foreach ($this->fieldSelectors() as $field_name => $selector) {
        $field_data = $current;
        $field_selectors = explode('/', trim($selector, '/'));
        foreach ($field_selectors as $field_selector) {
          if (is_array($field_data) && array_key_exists($field_selector, $field_data)) {
            $field_data = $field_data[$field_selector];
          }
          else {
            $field_data = '';
          }
        }
        // Extends xpath syntax compatibility to dot childs.
        if (empty($field_data)) {
          $subKeys = explode('.', $selector);
          $subChild = $current;
          foreach ($subKeys as $key) {
            $subChild = &$subChild[$key];
          }
          $field_data = $subChild;
        }
        $this->currentItem[$field_name] = $field_data;
      }
      if (!empty($this->configuration['include_raw_data'])) {
        $this->currentItem['raw'] = $current;
      }
      $this->iterator->next();
    }
  }

}
