Migrate Youtube

INTRODUCTION
------------

This module provides a set of two migrate source plugin to migrate videos and 
playlists.

REQUIREMENTS
------------

This module requires the following module:

 * Migrate - https://www.drupal.org/project/migrate
 * Migrate Plus - https://www.drupal.org/project/migrate_plus

RECOMMENDED MODULES
-------------------

 * Migrate Tools - https://www.drupal.org/project/migrate_tools

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 1. In your local settings.php file, add your Youtube API key as follow:

    $settings['youtube_api_key'] = 'your-own-1234567-API-KEY-here';

Migrating videos
----------------

Use the 'migrate_youtube_api_playlist_items' source plugin,
you can pass any youtube playlist ID as migrate source.

    source:
      plugin: migrate_youtube_api_playlist_items
      playlist_id: youtube-account-playlist-id

Because of API calls limitations on your key, this source tries to be as 
efficient as possible.
Additional calls are needed to retrieve video duration, if needed, set the 
contentDetails key to true

    source:
      plugin: migrate_youtube_api_playlist_items
      playlist_id: youtube-account-playlist-id
      contentDetails: true

Then add the duration to your destination as follow

    process:
      ...
      field_duration: duration

See the provided example in migration_examples/youtube_video_to_media.yml

Migrating playlist
------------------

You can retrieve all playlists from a specific user as follow:

    source:
        plugin: migrate_youtube_api_playlist
        account_id: UC6tjnYtebuODogr6C-ac-6g

See the provided example in migration_examples/youtube_video_to_media.yml

Source fields
----------------

Those fields are included by default in your migrate_youtube sources.
You won't have to write the code below, it is implicit:

    source:
      fields:
        -
          name: id
          label: 'Internal Youtube API item ID'
          selector: id
        -
          name: title
          label: 'Video title on Youtube'
          selector: 'snippet.title'
        -
          name: 'description'
          label: 'Description'
          selector: 'snippet.description'
        -
          name: 'published_date'
          label: 'Published date'
          selector: 'snippet.publishedAt'

Source fields exclusive to videos:

    source:
      fields
        -
          name: 'video_id'
          label: 'Video ID'
          selector: 'snippet.resourceId.videoId'
        -
          name: 'playlist_id'
          label: 'Playlist ID'
          selector: 'snippet.playlistId'

Overrides
---------

Any configuration added in your own yaml migration file will take precedence 
over the one defined by migrate_youtube.

Also, you can easily extend the YoutubeApiUrl.php class if you need more.

If you need more contentDetails fields (such as duration), you can do so by
extending the YoutubeApiPlaylistItemsUrl class and implementing your own
prepareRow method.
