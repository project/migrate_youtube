In your own module custom, rename the migrations_examples directory to migrations
and alter provided yaml configuration files to expose your custom migrate source to destination mappings.

We recommend using migrate tools with drush commands (https://www.drupal.org/project/migrate_tools)

Clear the cache, then list available migrations with:

`` drush ms

Execute a migration with:

`` drush mim {migration_name}
